%global srcname rosdistro

Name:           python-%{srcname}
Version:        0.9.0
Release:        3%{?dist}
Summary:        File format for managing ROS Distributions

License:        BSD and MIT
URL:            http://www.ros.org/wiki/rosdistro
Source0:        %{version}.tar.gz

Patch0:         0-test-index-increase-http-server-startup-wait-time.patch

BuildArch:      noarch

%description
The rosdistro tool allows you to get access to the full dependency tree and
the version control system information of all packages and repositories. To
increase performance, the rosdistro tool will automatically look for a cache
file on your local disk. If no cache file is found locally, it will try to
download the latest cache file from the server. The cache files are only used
to improve performance, and are not needed to get correct results. rosdistro
will automatically go to Github to find any dependencies that are not part
of the cache file. Note that operation without a cache file can be very slow,
depending on your own internet connection and the response times of Github.
The rosdistro tool will always write the latest dependency information to a
local cache file, to speed up performance for the next query.


%package doc
Summary:        HTML documentation for '%{name}'
BuildRequires:  make
BuildRequires:  python%{python3_pkgversion}-catkin-sphinx
BuildRequires:  python%{python3_pkgversion}-sphinx

%description doc
HTML documentation for the '%{srcname}' python module


%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{summary}
BuildRequires:  git
BuildRequires:  python%{python3_pkgversion}-catkin_pkg
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  python%{python3_pkgversion}-PyYAML
BuildRequires:  python%{python3_pkgversion}-rospkg
BuildRequires:  python%{python3_pkgversion}-setuptools
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
Obsoletes:      python2-%{srcname} < 0.7.4-4

%if %{undefined __pythondist_requires}
Requires:       python%{python3_pkgversion}-catkin_pkg
Requires:       python%{python3_pkgversion}-PyYAML
Requires:       python%{python3_pkgversion}-rospkg
Requires:       python%{python3_pkgversion}-setuptools
%endif

%description -n python%{python3_pkgversion}-%{srcname}
The rosdistro tool allows you to get access to the full dependency tree and
the version control system information of all packages and repositories. To
increase performance, the rosdistro tool will automatically look for a cache
file on your local disk. If no cache file is found locally, it will try to
download the latest cache file from the server. The cache files are only used
to improve performance, and are not needed to get correct results. rosdistro
will automatically go to Github to find any dependencies that are not part
of the cache file. Note that operation without a cache file can be very slow,
depending on your own internet connection and the response times of Github.
The rosdistro tool will always write the latest dependency information to a
local cache file, to speed up performance for the next query.


%prep
%autosetup -p1 -n %{srcname}-%{version}

# Drop unsupported syntax in older setuptools
sed -i "s/mock; python_version < '3.3'//" setup.py


%build
%py3_build

PYTHONPATH=$PWD/src \
  %make_build -C doc html SPHINXBUILD=sphinx-build-%{python3_version} SPHINXAPIDOC=sphinx-apidoc-%{python3_version}
rm doc/_build/html/.buildinfo


%install
%py3_install

# backwards compatibility symbolic links
pushd %{buildroot}%{_bindir}
for i in *; do
  ln -s ./$i python%{python3_pkgversion}-$i
done
popd


%check
PYTHONPATH=%{buildroot}%{python3_sitelib} \
  %{__python3} -m pytest \
  -k 'not test_manifest_providers' \
  test


%files doc
%license LICENSE.txt
%doc doc/_build/html

%files -n python%{python3_pkgversion}-%{srcname}
%license LICENSE.txt
%doc README.md
%{python3_sitelib}/%{srcname}/
%{python3_sitelib}/%{srcname}-%{version}-py%{python3_version}.egg-info/
%{_bindir}/rosdistro_build_cache
%{_bindir}/rosdistro_freeze_source
%{_bindir}/rosdistro_migrate_to_rep_141
%{_bindir}/rosdistro_migrate_to_rep_143
%{_bindir}/rosdistro_reformat
%{_bindir}/python%{python3_pkgversion}-rosdistro_build_cache
%{_bindir}/python%{python3_pkgversion}-rosdistro_freeze_source
%{_bindir}/python%{python3_pkgversion}-rosdistro_migrate_to_rep_141
%{_bindir}/python%{python3_pkgversion}-rosdistro_migrate_to_rep_143
%{_bindir}/python%{python3_pkgversion}-rosdistro_reformat


%changelog
* Thu Jan 23 2025 Dingli Zhang <dingli@iscas.ac.cn> - 0.9.0-3
- Add missing changelog to fix ci check

* Fri Jul 7 2023 SiHuaN <liyongtai@iscas.ac.cn> - 0.9.0-2
- Fix build error on riscv64

* Wed May 3 2023 will_niutao <niutao2@huawei.com> - 0.9.0-1
- Init for openEuler
